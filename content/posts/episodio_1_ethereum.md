---
title: "Episodio 1: Blockchain públicas, Ethereum"
date: 2022-01-25T11:00:00+00:00
# weight: 1
# aliases: ["/first"]
tags: ["Blockchain", "Blockchain públicas", "Ethereum"]
author: "Juan Martín"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: true
description: "Episodio donde hablamos sobre las blockchains públicas poniendo de ejemplo a la red Ethereum"
canonicalURL: "https://canonical.url/to/page"
disableHLJS: false # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: false
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShareButtons: ["twitter", "telegram", "whatsapp", "linkedin"]

cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://gitlab.com/Descentralizandopodcast/descentralizandopodcast.gitlab.io/-/tree/main/content"
    Text: "Sugerir cambios" # edit text
    appendFilePath: false # to append file path to Edit link
---

{{<audio src-ogg="https://archive.org/download/e-1-ethereum/E1_Ethereum.ogg" src-mp3="https://archive.org/download/e-1-ethereum/E1_Ethereum.mp3" >}}

Episodio donde hablamos de las blockchains públicas y para ello nos serviremos de la blockchain Ethereum como ejemplo. Vamos a empezar contando que es una blockchain, como funciona y como es la topología clásica de estas redes, para ir desembocando en como podemos usar estas redes como creadores de aplicaciones descentralizadas.

Vemos en el episodio como podemos iniciarnos en el desarrollo de DAPPs y smart contracts, que herramientas tenemos disponibles y como es el flujo de trabajo si queremos empezar a crear con esta tecnología.

Fuentes: 

[https://ethereum.org/es/](https://ethereum.org/es/)

[https://ethereum.org/es/developers/](https://ethereum.org/es/developers/)

[https://trufflesuite.com/](https://trufflesuite.com/)

[https://trufflesuite.com/docs/ganache/](https://trufflesuite.com/docs/ganache/)

[https://ethereum-on-arm-documentation.readthedocs.io/en/latest/](https://ethereum-on-arm-documentation.readthedocs.io/en/latest/)

[https://www.youtube.com/watch?v=xQ5YkMBPe0o&t=597s](https://www.youtube.com/watch?v=xQ5YkMBPe0o&t=597s)

Música de [Coma-Media](https://pixabay.com/es/users/coma-media-24399569/?tab=audio&utm_source=link-attribution&utm_medium=referral&utm_campaign=audio&utm_content=11254) de [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=music&utm_content=11254)

