---
title: "Episodio 2: Blockchain privadas permisionadas, Hyperledger Fabric"
date: 2022-02-08T11:00:00+00:00
# weight: 1
# aliases: ["/first"]
tags: ["Blockchain", "Blockchain privadas", "Hyperledger", "Fabric"]
author: "Juan Martín"
# author: ["Me", "You"] # multiple authors
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: true
description: "Episodio en el que hablaremos sobre las blockchains privadas permisionadas y para ello tomaremos de ejemplo el proyecto Hyperledger Fabric"
canonicalURL: "https://canonical.url/to/page"
disableHLJS: false # to disable highlightjs
disableShare: false
disableHLJS: false
hideSummary: false
searchHidden: false
ShowReadingTime: false
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShareButtons: ["twitter", "telegram", "whatsapp", "linkedin"]

cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
editPost:
    URL: "https://gitlab.com/Descentralizandopodcast/descentralizandopodcast.gitlab.io/-/tree/main/content"
    Text: "Sugerir cambios" # edit text
    appendFilePath: false # to append file path to Edit link
---

{{<audio src-ogg="https://archive.org/download/e-2-bc-privadas-permisionadas/E2_BC_privadas_permisionadas.ogg" src-mp3="https://archive.org/download/e-2-bc-privadas-permisionadas/E2_BC_privadas_permisionadas.mp3" >}}

Episodio en el que hablaremos sobre las blockchains privadas permisionadas y para ello tomaremos de ejemplo el proyecto Hyperledger Fabric. Vemos como esta tecnología nos ayuda a solventar mucho de los problemas que nos encontramos con las blockchains públicas y nos permite abordar una abanico diferente de casos de uso.

Vemos la topología de una red de Hyperledger Fabric, sus componentes principales y como podemos usar estar redes para construir soluciones sobre ella.

Fuentes: 

[https://www.hyperledger.org/use/fabric](https://www.hyperledger.org/use/fabric)

[https://hyperledger-fabric.readthedocs.io/en/release-2.2/](https://hyperledger-fabric.readthedocs.io/en/release-2.2/)

